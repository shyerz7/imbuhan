package com.example.gaga.bipa;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.content.Intent;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    TextView textView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_layout);
        textView = (TextView) findViewById(R.id.text);
    }

    public void buttonPress(View v) {
        switch (v.getId()) {
            case R.id.btnAwalan:
                Intent awalan = new Intent(this, AwalanActivity.class);
                startActivity(awalan);
                break;
            case R.id.btnAkhiran:
                Intent akhiran = new Intent(this, AkhiranActivity.class);
                startActivity(akhiran);
                break;
            case R.id.btnInformasi:
                Intent informasi = new Intent(this, InformasiActivity.class);
                startActivity(informasi);
                break;
            case R.id.btnBudaya:
                Intent budaya = new Intent(this, KebudayaanActivity.class);
                startActivity(budaya);
                break;
            case R.id.btnKataDepan:
                Intent kata_depan = new Intent(this, KataDepanActivity.class);
                startActivity(kata_depan);
                break;
            case R.id.btnGabungan:
                Intent gabungan = new Intent(this, GabunganActivity.class);
                startActivity(gabungan);
                break;
            case R.id.btnPermainan:
                Intent permainan = new Intent(this, PermainanActivity.class);
                startActivity(permainan);
                break;
        }
    }

}
